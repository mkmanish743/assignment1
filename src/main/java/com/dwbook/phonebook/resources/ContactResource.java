package com.dwbook.phonebook.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import com.dwbook.phonebook.representations.Contact;

@Path("/contact")
@Produces(MediaType.APPLICATION_JSON)
public class ContactResource {
    //method for obtaining information regarding the stored contact

    @GET
    @Path("/{id}")
    public Response getContact(@PathParam("id") int id) {
        return Response.ok(new Contact(id,"Manish","Kumar","91-2454644"))
                .build();
    }
    //methods for creating, deleting and updating contacts.
    //Since nothing is appended to our base URI, this method does not need to be annotated with @Path .
    @POST
    public Response createContact(Contact contact)
    {
        //store the new contact
        return Response
                .created(null)
                .build();
    }

    @PUT
    @Path("/{id}")
    public Response updateContact(
            @PathParam("id") int id,
            Contact contact){
        //Update the contact with the provided ID
        return Response
                .ok(new Contact(id,contact.getFirstName(),contact.getLastName(),contact.getPhone()))
                .build();
    }


}

